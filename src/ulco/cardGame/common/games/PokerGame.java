package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.PokerBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.io.*;
import java.net.Socket;
import java.util.*;

public class PokerGame extends BoardGame {
    //Déclaration des variables globales
    private List<Card> cards;
    private List<Coin> coins;
    private Integer maxRounds = 3;
    private Integer numberOfRounds = 0;

    /**
     * Constructeur
     *
     * @param name
     * @param maxPlayers
     * @param filename
     */
    public PokerGame(String name, Integer maxPlayers, String filename) {
        super(name, maxPlayers, filename);
        this.board = new PokerBoard();
    }

    /**
     * Initialisation des listes (sensiblement la même chose que pour CardGame)
     *
     * @param filename
     */
    public void initialize(String filename) {
        try {
            File cardFile = new File(filename);
            Scanner myReader = new Scanner(cardFile);
            this.cards = new ArrayList<>();
            this.coins = new ArrayList<>();
            while (myReader.hasNextLine()) {
                String[] data = myReader.nextLine().split(";");
                if (data[0].compareTo("Card") == 0) {
                    this.cards.add(new Card(data[1], Integer.parseInt(data[2]), true));
                } else if (data[0].compareTo("Coin") == 0) {
                    this.coins.add(new Coin(data[1], Integer.parseInt(data[2])));
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * Methode de jeu
     *
     * @return le WINNER
     */
    public Player run(Map<Player, Socket> connectedPlayers) throws IOException, ClassNotFoundException {
        Player winner = null;
        Player round = null;
        numberOfRounds = 0;
        for (Player player : players) {
            for (Coin coin : coins) {
                player.addComponent(coin);
            }
        }

        List<Player> joueursCouches = new ArrayList<>();
        while (!end()) {
            numberOfRounds++;
            //Affichage du round
            for (Player player : players){
                ObjectOutputStream sortie = new ObjectOutputStream(connectedPlayers.get(player).getOutputStream());
                sortie.writeObject("Round " + numberOfRounds);
            }
            //Melange des cartes
            Collections.shuffle(cards);

            for (Player player : players){
                ObjectOutputStream sortie = new ObjectOutputStream(connectedPlayers.get(player).getOutputStream());
                sortie.writeObject(this);
            }
            for (int i = 0; i < 3 * (players.size() + 1); i++) {
                if (i < 3 * players.size()) {
                    players.get(i % players.size()).addComponent(cards.get(i));
                } else {
                    board.addComponent(cards.get(i));
                }
            }

            for (int i = 0; i < 3; i++) {
                for (Player player : players) {
                    if (player.getScore() == 0)
                    {
                        joueursCouches.add(player);
                    }
                    if (((players.size() - joueursCouches.size()) == 1) && !joueursCouches.contains(player)) {
                        round = player;
                        break;
                    } else if (!joueursCouches.contains(player)) {
                        for (Player joueur : players){
                            ObjectOutputStream sortie = new ObjectOutputStream(connectedPlayers.get(joueur).getOutputStream());
                            sortie.writeObject(board);
                        }
                        for (Player joueur : players){
                            ObjectOutputStream sortie = new ObjectOutputStream(connectedPlayers.get(joueur).getOutputStream());
                            if(joueur.equals(player)){
                                sortie.writeObject("["+joueur.getName()+"] you have to play ...");
                                ObjectOutputStream sortie2 = new ObjectOutputStream(connectedPlayers.get(player).getOutputStream());
                                sortie2.writeObject(this);
                            }
                            else {
                                sortie.writeObject("Waiting for "+player.getName()+" to play ...");
                            }
                        }
                        ObjectInputStream entree = new ObjectInputStream(connectedPlayers.get(player).getInputStream());
                        Coin jeton = (Coin) entree.readObject();
                        if (jeton != null) {
                            for (Player joueur : players){
                                ObjectOutputStream outputStream = new ObjectOutputStream(connectedPlayers.get(joueur).getOutputStream());
                                outputStream.writeObject(player.getName() + " bet " + jeton.getName() + " coin");
                            }
                            System.out.println("PG "+jeton);
                            board.addComponent(jeton);
                            player.removeComponent(jeton);
                        } else {
                            joueursCouches.add(player);
                        }
                    }
                }
                for(Component component : board.getSpecificComponents(Card.class)){
                    Card carte = (Card) component;
                    carte.setHidden(false);
                }
            }
            if (round == null) {
                Integer apparitionWinner  = 0;
                Integer carteWinner  = 0;
                for (Player player : players) {
                    Integer apparitions  = 1;
                    Integer vCard  = 0;
                    if (!joueursCouches.remove(player)) {

                        List<Component> mainPlayer = player.getSpecificComponents(Card.class);
                        List<Integer> playerHand = new ArrayList<>();
                        for (Component card : mainPlayer)
                        {
                            playerHand.add(card.getValue());
                        }
                        for (Component carte : board.getSpecificComponents(Card.class)) {
                            playerHand.add(carte.getValue());
                        }
                        //Pour chaque valeur de cartes
                        for (Integer value : playerHand) {
                            //Récupèration du nombre d'apparition
                            int cardFrequency = Collections.frequency(playerHand, value);
                            //nombre d'apparitions maximum
                            if (cardFrequency > apparitions ) {
                                //on change le nombre d'apparitions et la valeur
                                apparitions  = cardFrequency;
                                vCard  = value;
                                //Si le nombre d'apparitionest le même mais que la valeur est supérieur
                            } else if (cardFrequency == apparitions  && value > vCard ) {
                                //on change juste la valeur
                                vCard  = value;
                            }
                        }
                        //Si il y a plus d'apparition chez le joueur que chez le gagnant
                        if (apparitions  > apparitionWinner) {
                            //On prend les valeurs du nouveau gagnant
                            round = player;
                            apparitionWinner = apparitions ;
                            carteWinner = vCard ;
                        }//Sinon, si le nbr d'apparition est pareil mais que les valeurs des cartes est différente
                        else if (apparitions .equals(apparitionWinner) && vCard  > carteWinner) {
                            //Le gagnant est bien le joueur et on change la valuer la plus haute
                            round = player;
                            carteWinner = vCard ;
                            //Sinon joueur aléatoire
                        } else if (apparitions .equals(apparitionWinner) && vCard .equals(carteWinner) && Math.random() > 0.5) {
                            round = player;
                        }
                        for (Player player1 : players){
                            ObjectOutputStream sortie = new ObjectOutputStream(connectedPlayers.get(player1).getOutputStream());
                            sortie.writeObject(player.getName() + " has " + apparitions  + " same card(s) of value " + vCard );
                        }
                    }
                }
                for (Player player : players){
                    ObjectOutputStream sortie = new ObjectOutputStream(connectedPlayers.get(player).getOutputStream());
                    sortie.writeObject(round.getName() + " win with " + apparitionWinner + " same card(s) of value " + carteWinner);
                }
            }


            for (Component coin : board.getSpecificComponents(Coin.class)) {
                round.addComponent(coin);
            }
            board.clear();
            for (Player player : players) {
                player.clearHand();
            }
            joueursCouches.clear();
        }


        int score = 0;
        for (Player player : players) {
            if (player.getScore() > score) {
                score = player.getScore();
                winner = player;
            } else if (player.getScore() == score && Math.random() > 0.5) {
                winner = player;
            }
        }
        return winner;

    }

    public boolean end() {
        return numberOfRounds.equals(maxRounds);
    }
}
