package ulco.cardGame.common.games.components;

public class Card extends Component{
    private boolean hidden;

    /**
     * Constructeur qui set la variable à hidden
     * @param name
     * @param value
     * @param hidden
     */
    public Card(String name, Integer value, boolean hidden) {
        super(name, value);
        this.hidden=hidden;
    }

    /**
     * Getter de hidden
     * @return hidden
     */
    public boolean isHidden(){
        return hidden;
    }

    /**
     * setter de Hidden
     * @param hidden
     */
    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    /**
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Card{" +
                "hidden=" + hidden +
                ", name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
