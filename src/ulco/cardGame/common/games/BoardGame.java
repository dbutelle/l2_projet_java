package ulco.cardGame.common.games;

import ulco.cardGame.common.interfaces.Board;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.players.BoardPlayer;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public abstract class BoardGame implements Game {
    protected String name;
    protected Integer maxPlayers;
    protected List<Player> players;
    protected boolean endGame;
    protected boolean started;
    protected Board board;
    /**
     * Constructeur qui instancie le joueur, le nom, le nombre de player max, et initialise le fichier de jeu
     * @param name
     * @param maxPlayers
     * @param filename
     */
    public BoardGame(String name, Integer maxPlayers, String filename) {
        this.name = name;
        this.maxPlayers = maxPlayers;
        this.players = new ArrayList<>();

        // initialize the Board Game using
        this.initialize(filename);
    }

    /**
     * Ajout d'un joueur à partir de certaines conditions
     * @param player
     * @return bool
     */
    public boolean addPlayer(Socket socket, Player player) throws IOException {

        String msg;
        if (players.size() == maxPlayers) {
            msg="You can't connect ! Max numbers of players reached.";
            ObjectOutputStream sortie1 = new ObjectOutputStream(socket.getOutputStream());
            sortie1.writeObject(msg);
            ObjectOutputStream sortie2 = new ObjectOutputStream(socket.getOutputStream());
            sortie2.writeObject("END");
            return false;
        }
        for (Player joueur : players) {
            if (joueur.getName().compareTo(player.getName())==0) {
                msg="You can't connect ! Your name already exist.";
                ObjectOutputStream sortie1 = new ObjectOutputStream(socket.getOutputStream());
                sortie1.writeObject(msg);
                ObjectOutputStream sortie2 = new ObjectOutputStream(socket.getOutputStream());
                sortie2.writeObject("END");
                return false;
            }
        }
        this.players.add(player);
        return true;

    }

    /**
     * Retrait d'un joueur de la liste
     * @param player
     */
    public void removePlayer(Player player) {
        players.remove(player);
    }

    /**
     * Suppression de toute la liste
     */
    public void removePlayers() {
        for (Player joueurs : players) {
            players.remove(joueurs);
        }
    }

    /**
     * Affichage des states
     */
    public void displayState() {
        System.out.println("-------------------------------------------");
        System.out.println("--------------- Game State ----------------");
        System.out.println("-------------------------------------------");
        for(Player joueurs : this.players){
            System.out.println(joueurs.toString());
        }
        System.out.println("-------------------------------------------");
    }

    /**
     * Getter de started
     * @return started
     */
    public boolean isStarted() {
        this.started = this.players.size() == maxPlayers;
        return this.started;

    }

    /**
     * Getter de maxPlayers
     * @return maxPlayers
     */
    public Integer maxNumberOfPlayers() {
        return this.maxPlayers;
    }

    /**
     * Getter de la liste des joueurs
     * @return players
     */
    public List<Player> getPlayers() {
        return this.players;
    }
    public Board getBoard(){return this.board;}

    public Player getCurrentPlayer(String username) {
        for (Player player : this.players){
            if (player.getName().compareTo(username) == 0) return player;
        }
        return null;
    }

}
